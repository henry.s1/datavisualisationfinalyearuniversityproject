﻿		
// function to initialise the visualisation on the webpage
function initialiseVisualisation()
{
	var containerDiv = document.getElementById("vizContainer"), // get the visualisation container from the website
		url = "https://public.tableau.com/views/UKAccidents2016Visualisation/Main", // assign the link to the visualisation
		options = 
		{
			hideTabs: false,	// show the tabs
			onFirstInteractive: function()
			{
				console.log("Visualisation Successfully loaded."); // print to show load is successful
			}
		};				
	var viz = new tableau.Viz(containerDiv, url, options); // initialise the visualisation from the information given
}

function testViz()
{
	initialiseVisualisation();
	
	
	
}

		
// function for loading the home page
function button1()
{
	showHide();  // show the tab if hidden else hide the tab if one is shown
	
	// text content to be shown on the website
	var p1 = "Welcome to the UK Road Traffic Accident Visualisation Tool.";
	
	var p2 = "This tool allows users to explore road traffic accidents that have "
			+"occurred in the United Kingdom during 2016. If this is the first time "
			+ "the tool is being used then please read the instructions tab." 
			+ "For more information about this application open the About tab. To download the data relating "
			+ "to this tool select the Data tab. To hide this text section press any of the tabs again.";
				
	var p3 = "To get started, please select a county in the map selector below.";
	
	// change the text content on the website to the above
	document.getElementById("p1").innerHTML = p1;
	document.getElementById("p2").innerHTML = p2;
	document.getElementById("p3").innerHTML = p3;
}
		
		// function for loading the instructions page
		function button2()
		{
			showHide(); // show the tab if hidden else hide the tab if one is shown
			
			// text content to be shown on the website
			var p1 = '<p>When the visualisation is initialised, the main scene will be loaded. This scene compares the number of accidents in the United Kingdom under dangerous and safe conditions regarding road surfaces, lighting and weather. </p>'+
						'<img src="images/Mainaaa.PaNG" alt="compareRoad">'+
						'<p>The county selector allows the user to choose a county in the United Kingdom, there is also functionality to select multiple counties at once. This will make the graphs on the sheet specific to the county selected.  For example, showing the number of accidents in \"Greater London\" if that county was selected. </p>'+
						'<img src="images/countySelector.PNG" alt="county"> '+
						'<p>The number of accidents graph shows the user the number of accidents that has happened in selected counties over a period of one year throughout 2016. The number of accidents during each month is shown on the graph. </p>'+
						'<img src="images/numAcc.PNG" alt="numberAccidents"> '+
						'<p>The graph can also be drilled down on in the date hierarchy to show the number of accidents happening during specific days throughout the months, drilling can be done by clicking on the + symbol next to January. To just view all the months the – button should be pressed to go back one step in the hierarchy to show all months without the days. </p>'+
						'<img src="images/NumAccChartDays.PNG" alt=numDays ">'+
						'<p>The accidents over a period on this graph can be changed. Next to the graph there is two sliders. The first slider labelled as \"Select months\" can be changed so that only certain months are shown, the default is 1 to 12 months which represents January to December. The second slider labelled as \"Select days\" can be changed so that only certain days within the months are shown. The default is between 1 to 31 days however a month doesn\"t have 31 days such as April then only the number of days present in that specific month will be shown on the graph. </p>'+
						'<img src="images/daymonthslider.PNG" alt="slider">'+
						'<p>Three charts comparing the number of accidents happening under dangerous and safe conditons are also present, these charts show the number of accidents regarding road surface condition, weather condition and lighting condition. All three of these graphs will dynamically update depending on what filters are present, the default shows all accidents in the UK over 2016 however if specific counties are selected or days or time frames are selected then these graphs will change according to these filters. </p>'+
						'<img src="images/allComparison.PNG" alt="allComp">'+
						'<p>These graphs are also used to drill down further in the hierarchy to display another scene to display the further in the chosen condition, for example road surface conditions are split between Dry, flood over 4cm, frost or ice, snow, and wet or damp. To access this scene, click on one of the bars then select \"Drill down to \"specific condition\". </p>'+
						'<img src="images/roadcomdrill.PNG" alt="roadDrill">'+
						'<p>This is the scene that is shown if the user has drilled down to a specific condition, in this case the condition is road surface condition. This scene shows all accidents that has happened in selected counties and also in this case if they happened under safe conditions \"Dry\" or Dangerous conditions  \"Flood over 4cm, Frost or Ice, Snow, Wet or Damp\". </p>'+
						'<img src="images/cumbriaAll.PNG" alt="cumbriaMain">'+
						'<p>The selected County: shows the counties that was specified in the previous main scene, \"Cumbria\" was selected so only this county is shown. </p>'+
						'<img src="images/selectedCumbria.PNG" alt="cumbria">'+
						'<p>As stated previously, this scene is the result of drilling down on a specific overall condition on the main page. This scene provides the user with the detailed comparison of a specific condition and not just if the accident happened under dangerous or safe conditions. In the case of road surface condition two graphs are shown. The first graph shows the number of accidents and the safe road surface condition which is \"dry\". The second graph shows the number of accidents and the dangerous conditions which are \"Flood over 4cm, Frost or ice, Snow, and Wet or damp\"</p>'+
						'<img src="images/cumbriaCompare.PNG" alt="cumbriaCom">'+
						'<p>The accidents in selected county map shows coordinates on the name specific to the chosen county, in this case only the accidents that happened in \"Cumbria\" is shown. The user is able to select one of these points and the information on the accident will be displayed on the right of the graph. </p>'+
						'<img src="images/cumbriaAccidents.PNG" alt="cumbriaAcc">'+
						'<p>To return to the main scene, simply click the back arrow located about the \"Return to main\" text, this will bring the user back to the \"Comparison of accidents under dangerous and safe conditions\" scene. </p>'+
						'<img src="images/returntomain.PNG" alt="return">';
 
			// change the text content on the website to the above
			document.getElementById("p1").innerHTML = p1;
			document.getElementById("p2").innerHTML = '';
			document.getElementById("p3").innerHTML = '';
		}
		
		// function for loading the about page
		function button3()
		{
			showHide(); // show the tab if hidden else hide the tab if one is shown
			
			// text content to be shown on the website
			var p1 = 'The UK government releases road safety data annually, they also release statistics based on this data. '
			+ 'The purpose of this tool is to visualise this data as there are no tools that has updated road traffic accident data.'
			+ '<p>There are some tools like ITOWorld\'s \"Road causalities UK\" tool however this only shows information about the people involved in'
			+ ' road traffic accidents from the years 2000 until 2010, the tool doesnt incorporate data visualisation concepts and techniques'
			+ ' like data drilling and isnt really\"interactive\" in the sense that users cannot change the way in which the data is viewed.</p>';
			
			// change the text content on the website to the above
			document.getElementById("p1").innerHTML = p1;
			document.getElementById("p2").innerHTML = '';
			document.getElementById("p3").innerHTML = '';
		}
		
		// function for loading the data page
		function button4()
		{
			showHide(); // show the tab if hidden else hide the tab if one is shown
			
			// text content to be shown on the website
			var p1 = 'This visualisation tool uses two separate data sets. The first set comes from the UK government website and is about road traffic '+
				+ 'accidents in the UK during 2016 which is available as a CSV file, the cleansed data can also be downloaded. The second data set is the reverse geocodeing of the longitude and latitude points'
				+ 'of the accidents in the first data set, these are present as separate XML files one for each accident.';
			var p2 = 'Download for the accident data: <a href="downloads/Accidents2016.csv" download\> Accident Data </a>' +
			'<p>Download for the accident data which has been cleansed: <a href="downloads/cleanseddata.zip" download\> Clensed Data </a><p>';
			var p3 = 'Download for the reverse geocoding data: <a href="downloads/alldata.zip" download\> Reverse Geocode data</a>';
			
			// change the text content on the website to the above
			document.getElementById("p1").innerHTML = p1;
			document.getElementById("p2").innerHTML = p2;
			document.getElementById("p3").innerHTML = p3;
		}
		
// function to show/hide the current text content
function showHide()
{
	var element = document.getElementById("TextContent");
	
	if(element.style.display === "block") 
	{
		element.style.display = 'none'; // set to hidden if shown
	}
	else
	{
		element.style.display = 'block'; // set to show if hidden
	}
}
		
		
		
		
		