package test;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

public class parseXMLT 
{
	
	
	private static ArrayList<String> getData(String fileName)
	{
		ArrayList<String> tempContainer = new ArrayList<String>();
		BufferedReader br = null;
		try
		{
			br = new BufferedReader(new FileReader(fileName));
			String line ="";
			while((line = br.readLine()) != null)
			{
				if(line.contains("﻿"))	line = line.replace("﻿", "");
				tempContainer.add(line);
			}				
		}
		catch(Exception e){	e.printStackTrace(); }
		finally
		{
			try{ br.close(); }
			catch(IOException ioe)
			{
				System.out.println("Error occured while closing the BufferedReader");
                ioe.printStackTrace();
			}
		}				
		return tempContainer;
	}

	static ArrayList<File> getFiles(String fileLocation)
	{
		ArrayList<File> xmlFiles = new ArrayList<File>();
		for(int i = 0; i < 136621; i++) xmlFiles.add(new File(fileLocation+ i + ".xml"));
		return xmlFiles;
	}
	
	static Boolean checkCounty(String county)
	{
		String allCounty = "Bedfordshire,Berkshire,City of Bristol,Buckinghamshire,Cambridgeshire,Cheshire,Cornwall,Cumbria,Derbyshire,Devon"
				+ ",Dorset,Durham,County Durham,East Sussex,Essex,Gloucestershire,Greater London,Greater Manchester,Hampshire,Herefordshire"
				+ ",Hertfordshire,Isle of Wight,Kent,Lancashire,Leicestershire,Lincolnshire,City of London,Merseyside,Norfolk,Northamptonshire"
				+ ",Northumberland,North Yorkshire,Nottinghamshire,Oxfordshire,Rutland,Shropshire,Somerset,South Yorkshire,Staffordshire,Suffolk"
				+ ",Surrey,Tyne and Wear,Warwickshire,West Midlands,West Sussex ,West Yorkshire,Wiltshire,Worcestershire,East Riding of Yorkshire"
				+ ", City of Nottingham, City of Edinburgh, Glasgow City, Southampton" 
				+ "City of Manchester, City of Salford, Bolton, Bury, Oldham, Rochdale, Stockport, Tameside, Trafford, Wigan"
				+ "City of Liverpool, Knowsley, St Helens, Sefton, Wirral"
				+ "City of Sheffield, Barnsley, Doncaster, Rotherham"
				+ "City of Newcastle upon Tyne, City of Sunderland, Gateshead, South Tyneside, North Tyneside"
				+ "City of Birmingham, City of Coventry, City of Wolverhampton, Dudley, Sandwell, Solihull, Walsall"
				+ "City of Leeds, City of Bradford, City of Wakefield, Calderdale, Kirklees";
		
		if(allCounty.contains(county))	return true;
		return false;
	}
	
	static ArrayList<Document> getLocations(ArrayList<File> xmlFiles)
	{
		ArrayList<Document> xmlLocations = new ArrayList<Document>();
		DocumentBuilderFactory docBuilderFactory = DocumentBuilderFactory.newInstance();
        DocumentBuilder docBuilder;
        
		try
		{
			docBuilder = docBuilderFactory.newDocumentBuilder();
			for(int h = 0; h < xmlFiles.size(); h++)
			{
				try
				{
					Document doc = docBuilder.parse(xmlFiles.get(h));
					xmlLocations.add(doc);
					String temp = xmlFiles.get(h).getName();
					temp = temp.replace(".xml", "");
				}
				catch(IOException e) { xmlLocations.add(null); }
			}
			
		} catch (ParserConfigurationException | SAXException e){ e.printStackTrace(); }
		
		
		return xmlLocations;
	}
	
	static void outputData(String dataOutputLocation, String nullOutputLocation, ArrayList<Document> xmlContainer, ArrayList<String> strContainer, ArrayList<String> accidentContainer)
	{
		PrintWriter pw;
		PrintWriter pwT;
		try {
			pw = new PrintWriter(new File(dataOutputLocation));
			pwT = new PrintWriter(new File(nullOutputLocation));
			
			String headings = "Accident Index, Longitude, Latitude, Accident Severity, Number of Vehicles, Number of Casualties, Date,"
					+ "Road Type, Speed Limit, PC Human Control, PC Physical Facilities, Lighting Condition,"
					+ "Weather Condition, Road Surface Condition, County\n";
			
			Map<String, String> accSev = new HashMap<String, String>();
			accSev.put("1", "Fatal");
			accSev.put("2", "Serious");
			accSev.put("3", "Slight");		

			Map<String, String>  roadType = new HashMap<String, String> ();
			roadType.put("1", "Roundabout");
			roadType.put("2", "One Way Street");
			roadType.put("3", "Dual Carriageway");
			roadType.put("6", "Single Carriageway");
			roadType.put("7", "Slip Road");
			roadType.put("12", "One Way Street / Slip Road");
			
			Map<String, String>  pcHuman = new HashMap<String, String> ();
			pcHuman.put("0", "None within 50m");
			pcHuman.put("1", "Control by School Crossing Patrol");
			pcHuman.put("2", "Control by Other Authorised Person");
			
			Map<String, String>  pcPhyFac = new HashMap<String, String> ();
			pcPhyFac.put("0", "No physical crossing facilities within 50 metres");
			pcPhyFac.put("1", "Zebra");
			pcPhyFac.put("4", "Pelican/puffin/toucan or similar non-junction pedestrian light crossing");
			pcPhyFac.put("5", "Pedestrian phase at traffic signal junction");
			pcPhyFac.put("7", "Footbridge or subway");
			pcPhyFac.put("8", "Central Refuge");
			
			Map<String, String>  lightCon = new HashMap<String, String> ();
			lightCon.put("1", "Daylight");
			lightCon.put("4", "Darkness - lights lit");
			lightCon.put("5", "Darkness - lights unlit");
			lightCon.put("6", "Darkness - no lights");
			lightCon.put("7", "Darkness - unknown");
			
			Map<String, String>  weatCon = new HashMap<String, String> ();
			weatCon.put("1", "Fine - no high winds");
			weatCon.put("2", "Raining - no high winds");
			weatCon.put("3", "Fine - high winds");
			weatCon.put("4", "Raining  - high winds");
			weatCon.put("5", "Snowing  - high winds");
			weatCon.put("7", "Fog or mist");		
			
			Map<String, String>  roadSurCon = new HashMap<String, String> ();
			roadSurCon.put("1", "Dry");
			roadSurCon.put("2", "Wet or damp");
			roadSurCon.put("3", "Snow");
			roadSurCon.put("4", "Frost or ice");
			roadSurCon.put("5", "Flood over 4cm");
			roadSurCon.put("6", "Oil or diesel");
			roadSurCon.put("7", "Mud");
			
			
			
			pw.write(headings);
			pwT.write(headings);
			
			for(int k = 1; k < xmlContainer.size();k++)
			{
				StringBuilder temp = new StringBuilder();
				
				temp.append("A"+k); // assign index as Ak where k is the line number
				temp.append(",");
				
				String[] locationData = (accidentContainer.get(k)).split(",");
				temp.append(locationData[3]); // long
				temp.append(",");
				temp.append(locationData[4]); // lat
				temp.append(",");
				temp.append(accSev.getOrDefault(locationData[6], "null")); // acc sev
				temp.append(",");
				temp.append(locationData[7]); // # of vehicles
				temp.append(",");
				temp.append(locationData[8]); // # of cas
				temp.append(",");
				temp.append(locationData[9]); // date
				temp.append(",");
				temp.append(roadType.getOrDefault(locationData[16], "null")); // road type
				temp.append(",");
				temp.append(locationData[17]); // speed limit
				temp.append(",");
				temp.append(pcHuman.getOrDefault(locationData[22], "null")); // ped cross human control
				temp.append(",");
				temp.append(pcPhyFac.getOrDefault(locationData[23], "null")); // ped cross physical facility
				temp.append(",");
				temp.append(lightCon.getOrDefault(locationData[24], "null")); // light con
				temp.append(",");
				temp.append(weatCon.getOrDefault(locationData[25], "null")); // weather con
				temp.append(",");
				temp.append(roadSurCon.getOrDefault(locationData[26], "null")); // road surface con
				
				
				temp.append(",");
				temp.append(strContainer.get(k));
				temp.append("\n");
				
				String finalString = temp.toString();
				if(!finalString.contains("null") && !finalString.contains("NULL") && !finalString.contains("Null") && !strContainer.get(k).matches(""))
				{
					pw.write(finalString);
					System.out.println("Written: "+ k);
				}
				else
				{
					pwT.write(finalString);
				}
				
			}
			pw.close();
			
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	static boolean getCountyByTag(Element eElement, StringBuilder stringBuilder, String tag)
	{
		if(eElement.getElementsByTagName(tag).item(0)!= null)
 	   {
     	   if(checkCounty(eElement.getElementsByTagName(tag).item(0).getTextContent()))
     	   {
     		   if(eElement.getElementsByTagName(tag).item(0).getTextContent().matches("London"))
     		   {
     			   stringBuilder.append("Greater London");
     			   return true;
     		   }
     		   else
     		   {
     			   stringBuilder.append(eElement.getElementsByTagName(tag).item(0).getTextContent());		
     			   return true;
     		   }
     	   }
 	   }
		
		return false;
	}
	
	public static void main(String[] args) 
	{		
		String fileLocation = "G:\\PC\\Desktop\\alldata\\";
		ArrayList<Document> xmlContainer = getLocations(getFiles(fileLocation));
		ArrayList<String> strContainer = new ArrayList<String>();		
		ArrayList<String> accidentContainer = getData("G:\\PC\\Desktop\\acc.csv");
		
		for(int i = 0 ; i < xmlContainer.size(); i++)
		{			
			Document temp = xmlContainer.get(i);
			temp.getDocumentElement().normalize();
			
			NodeList nList = temp.getElementsByTagName("addressparts");
			StringBuilder sb = new StringBuilder();
			
			for (int j = 0; j < nList.getLength(); j++) 
			{
				if(nList.item(j) != null)
				{
					Node nNode = nList.item(j);  
		            if (nNode.getNodeType() == Node.ELEMENT_NODE) 
		            {
		            	
		               Element eElement = (Element) nNode;
		               
		               if(eElement.getElementsByTagName("county").item(0) != null)
		               {		            	  
		            	   String tempCounty = eElement.getElementsByTagName("county").item(0).getTextContent();
		            	   
		            	   String metCountys = "Greater Manchester, Merseyside, South Yorkshire, Tyne and Wear, West Midlands Combined Authority, West Yorkshire";
		            	   
		            	   if(!metCountys.contains(tempCounty))
		            	   {
		            		   Map<String, String>  correctedCounty = new HashMap<String, String> ();
		            		   correctedCounty.put("Lancs", "Lancashire");
		            		   correctedCounty.put("Na h-Eileanan Siar", "Na h-Eileanan an Iar");
		            		   correctedCounty.put("Caithness", "Highland");
		            		   correctedCounty.put("Shapinsay", "Orkney");
		            		   correctedCounty.put("Oxon", "Oxfordshire");
		            		   
		            		   tempCounty = correctedCounty.getOrDefault(tempCounty, tempCounty);
		            		   
		            		   
		            		   sb.append(tempCounty);
		            		   break;
		            	   }
		               }
		               
		               
		               if(eElement.getElementsByTagName("state").item(0)!= null)
		               { 
		            	   if(eElement.getElementsByTagName("state").item(0).getTextContent().matches("Wales"))
		            	   {
		            		   if(eElement.getElementsByTagName("city").item(0) != null)
		            		   {
		            			   String city = eElement.getElementsByTagName("city").item(0).getTextContent();
		            			   
		            			   String clwyd = "Conwy, Denbighshire, Flintshire, Wrexham";
		            			   String dyfed = "Carmarthenshire, Ceredigion, Pembrokeshire";
		            			   String gwynedd = "Gwynedd, Isle of Anglesey";
		            			   String midGlamorgan = "Bridgend, Merthyr Tydfil, Rhondda Cynon Taf";
		            			   String southGlamorgan = "Cardiff, Vale of Glamorgan";
		            			   String westGlamorgan = "Neath Port Talbot, Swansea";
		            			   
		            			   if(checkCounty(city))
		            			   {
		            				   if(clwyd.contains(city))
			            			   {
			            				   sb.append("Clwyd");
			            				   break;
			            			   }
			            			   else if(dyfed.contains(city))
			            			   {
			            				   sb.append("Dyfed");
			            				   break;
			            			   }
			            			   else if(gwynedd.contains(city)) 
			            			   {
			            				   sb.append("Gwynedd");
			            				   break;
			            			   }
			            			   else if(southGlamorgan.contains(city)) 
			            			   {
			            				   sb.append("South Glamorgan");
			            				   break;
			            			   }
			            			   else if(westGlamorgan.contains(city)) 
			            			   {
			            				   sb.append("West Glamorgan");
			            				   break;
			            			   }
			            			   else if(midGlamorgan.contains(city)) 
			            			   {		
			            				   sb.append("Mid Glamorgan");
			            				   break;
			            			   }
			            			   else
			            			   {
			            				   sb.append(city);
			            			   }
		            				   break;
		            			   }
		            		   }
		            	   }
		               }
		               
		               
		               
		               if(getCountyByTag(eElement, sb, "city"))
		               {
		            	   break;
		               }
		               else if(getCountyByTag(eElement, sb, "town"))
		               {
		            	   break;
		               }
		               else if(getCountyByTag(eElement, sb, "village"))
		               {
		            	   break;
		               }	               		               
		               
		               sb.append("null"); // county city town village not present cannot determine main county
		               
		            }
				}			
	         }
			String tempStr = sb.toString();
			strContainer.add(tempStr);
		}
		
		
		String dataOutputLocation = "G:\\PC\\Desktop\\tempF.csv";
		String nullOutputLocation = "G:\\PC\\Desktop\\null.csv";
		
		outputData(dataOutputLocation, nullOutputLocation, xmlContainer, strContainer, accidentContainer);
		
		


		
		
	}
	
}


