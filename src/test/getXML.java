package test;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.concurrent.TimeUnit;


public class getXML 
{

	private static ArrayList<String> getData(String fileName, int index)
	{		
		ArrayList<String> tempContainer = new ArrayList<String>();
		
		BufferedReader br = null;
		
		try
		{
			br = new BufferedReader(new FileReader(fileName));
			
			String line ="";
			
			while((line = br.readLine()) != null)
			{
				if(line.contains("﻿"))
				{
					line = line.replace("﻿", "");
				}
				String[] lineTemp = line.split(",");
				
				tempContainer.add(lineTemp[index]);
				
			}				
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
		finally
		{
			try
			{
				br.close();
			}
			catch(IOException ioe)
			{
				System.out.println("Error occured while closing the BufferedReader");
                ioe.printStackTrace();
			}
		}
				
		return tempContainer;
	}
	
	
	private static void getWebContent(URL input, File output) throws IOException 
	{
		
	    InputStream in = input.openStream();
	    BufferedReader bufferedReader;
	    String inputLine;
	    try 
	    {
	    	PrintWriter pw = new PrintWriter(output);
	    	try 
	    	{
	    		bufferedReader = new BufferedReader(new InputStreamReader(in));
	    		while((inputLine = bufferedReader.readLine()) != null)
	    		{
	    			pw.write(inputLine);
	    		}
	    	} 
	    	finally
	    	{
	    		pw.close();
	    	}
	    } 
	    finally 
	    {
	    	in.close();	
	    }
	}
	  
	
	
	private static String makeAddress(String latitude ,String longitude)
	{
		String i1 = "http://nominatim.openstreetmap.org/reverse.?format=xml&lat=";//"https://nominatim.openstreetmap.org/reverse?format=xml&lat=";   // for server use//"http://192.168.78.128/nominatim/reverse.php?format=xml&lat="; 
		String i2 = "&lon=";
		String i3 = "&zoom=18&addressdetails=1";
		StringBuilder sb = new StringBuilder();
		sb.append(i1);
		sb.append(latitude);
		sb.append(i2);
		sb.append(longitude);		
		
		return sb.toString();
	}
	
	private static String makeDestination(String fileNumber)
	{
		String fileLocation = "G:\\PC\\Desktop\\alldata\\";//"/home/henry/Documents/xml/";
		String fileExtention = ".xml";//".json";
		
		StringBuilder sbFile = new StringBuilder();
		sbFile.append(fileLocation);
		sbFile.append(fileNumber);
		sbFile.append(fileExtention);
		
		return sbFile.toString();
	}
	  
	public static void main(String[] args) 
	{		
		ArrayList<String> latitude1 = getData("G:\\PC\\Desktop\\Acc.csv", 4);
		ArrayList<String> longitude1 = getData("G:\\PC\\Desktop\\Acc.csv", 3);
		
		// loop through all of the coordinates getWebContenting all geolocation data
		for(int i = 9; i < 10; i++) 
		{
						
			// makes the url of the accident
			String urlString = makeAddress(latitude1.get(i), longitude1.get(i)); 
				
			// make the destination name for the accident
			String xmlFile = makeDestination(Integer.toString(i));
						
			// creates a new file to hold accident data
			File file = new File(xmlFile);			
			
			try
			{
				URL url = new URL(urlString);				
			    getWebContent(url, file); // download the location data for the coordinates
			}
			catch(IOException e)
			{
				System.err.println("Cannot create file");				
			}
		}		
	}

}
