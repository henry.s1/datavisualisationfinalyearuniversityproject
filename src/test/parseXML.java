package test;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileFilter;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Scanner;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

public class parseXML 
{
	private static ArrayList<String> getData(String fileName)
	{
		ArrayList<String> tempContainer = new ArrayList<String>();		
		BufferedReader br = null;
		
		try
		{
			br = new BufferedReader(new FileReader(fileName));
			
			String line ="";
			while((line = br.readLine()) != null)
			{
				if(line.contains("﻿"))
				{
					line = line.replace("﻿", "");
				}
				tempContainer.add(line);
				
				
			}				
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
		finally
		{
			try
			{
				br.close();
			}
			catch(IOException ioe)
			{
				System.out.println("Error occured while closing the BufferedReader");
                ioe.printStackTrace();
			}
		}
				
		return tempContainer;
	}
		
	static ArrayList<File> getFiles()
	{
		ArrayList<File> xmlFiles = new ArrayList<File>();
		for(int i = 0; i < 136621; i++)
		{
			xmlFiles.add(new File("G:\\PC\\Desktop\\alldata\\" + i + ".xml"));
		}		
		return xmlFiles;	}
		
	static Boolean checkMet(String county)
	{
		String allBoroughs = "City of Manchester, City of Salford, Bolton, Bury, Oldham, Rochdale, Stockport, Tameside, Trafford, Wigan"
				+ "City of Liverpool, Knowsley, St Helens, Sefton, Wirral"
				+ "City of Sheffield, Barnsley, Doncaster, Rotherham"
				+ "City of Newcastle upon Tyne, City of Sunderland, Gateshead, South Tyneside, North Tyneside"
				+ "City of Birmingham, City of Coventry, City of Wolverhampton, Dudley, Sandwell, Solihull, Walsall"
				+ "City of Leeds, City of Bradford, City of Wakefield, Calderdale, Kirklees";
		if(allBoroughs.contains(county))
		{
			return true;
		}
		
		return false;
	}
		
	static Boolean checkCounty(String county)
	{
		String allCounty = "Bedfordshire,Berkshire,City of Bristol,Buckinghamshire,Cambridgeshire,Cheshire,Cornwall,Cumbria,Derbyshire,Devon"
				+ ",Dorset,Durham,County Durham,East Sussex,Essex,Gloucestershire,Greater London,Greater Manchester,Hampshire,Herefordshire"
				+ ",Hertfordshire,Isle of Wight,Kent,Lancashire,Leicestershire,Lincolnshire,City of London,Merseyside,Norfolk,Northamptonshire"
				+ ",Northumberland,North Yorkshire,Nottinghamshire,Oxfordshire,Rutland,Shropshire,Somerset,South Yorkshire,Staffordshire,Suffolk"
				+ ",Surrey,Tyne and Wear,Warwickshire,West Midlands,West Sussex ,West Yorkshire,Wiltshire,Worcestershire,East Riding of Yorkshire"
				+ ", City of Nottingham, City of Edinburgh, Glasgow City, Southampton" 
				+ "City of Manchester, City of Salford, Bolton, Bury, Oldham, Rochdale, Stockport, Tameside, Trafford, Wigan"
				+ "City of Liverpool, Knowsley, St Helens, Sefton, Wirral"
				+ "City of Sheffield, Barnsley, Doncaster, Rotherham"
				+ "City of Newcastle upon Tyne, City of Sunderland, Gateshead, South Tyneside, North Tyneside"
				+ "City of Birmingham, City of Coventry, City of Wolverhampton, Dudley, Sandwell, Solihull, Walsall"
				+ "City of Leeds, City of Bradford, City of Wakefield, Calderdale, Kirklees";
		
		if(allCounty.contains(county))
		{
			return true;
		}		
		return false;
	}
		
	public static void main(String[] args) 
	{
		ArrayList<Document> docContainer = new ArrayList<Document>();
		ArrayList<String> strContainer = new ArrayList<String>();
		
		ArrayList<String> origData = getData("G:\\PC\\Desktop\\acc.csv");
				
		ArrayList<String> metCounty = new ArrayList<String>();
		metCounty.add("Greater Manchester");
		metCounty.add("Oxon");
		metCounty.add("West Midlands Combined Authority");
		metCounty.add("Tyne and Wear");
		
		ArrayList<Integer> intContainer = new ArrayList<Integer>();
		
		ArrayList<File> xmlFiles = getFiles();
		
		DocumentBuilderFactory docBuilderFactory = DocumentBuilderFactory.newInstance();
        DocumentBuilder docBuilder;
        
		try
		{
			docBuilder = docBuilderFactory.newDocumentBuilder();
			for(int h = 0; h < xmlFiles.size(); h++)
			{
				try
				{
					Document doc = docBuilder.parse(xmlFiles.get(h));
					docContainer.add(doc);
					String temp = xmlFiles.get(h).getName();
					temp = temp.replace(".xml", "");
					intContainer.add(Integer.parseInt(temp));	
				}
				catch(IOException e)
				{
					docContainer.add(null);
				}
			}
			
		} catch (ParserConfigurationException | SAXException e)
		{
			e.printStackTrace();
		}
       
		
		for(int i = 0 ; i < docContainer.size(); i++)
		{			
			System.out.println(i);
			Document temp = docContainer.get(i);
			temp.getDocumentElement().normalize();
			System.out.println("Root element :" + temp.getDocumentElement().getNodeName());
			NodeList nList = temp.getElementsByTagName("addressparts");
			StringBuilder sb = new StringBuilder();
			
			for (int j = 0; j < nList.getLength(); j++) 
			{
				if(nList.item(j) != null)
				{
					Node nNode = nList.item(j);  
		            if (nNode.getNodeType() == Node.ELEMENT_NODE) 
		            {		            	
		               Element eElement = (Element) nNode;
		               
		               if(eElement.getElementsByTagName("county").item(0) != null)
		               {		            	  
		            	   String tempCounty = eElement.getElementsByTagName("county").item(0).getTextContent();
		            	   
		            	   String metCountys = "Greater Manchester, Merseyside, South Yorkshire, Tyne and Wear, West Midlands Combined Authority, West Yorkshire";
		            	   
		            	   if(!metCountys.contains(tempCounty))
		            	   {
		            		   switch(tempCounty)
		            		   {
			            		   case "Lancs":
			            			   sb.append("Lancashire");
			            			   break;
			            		   case "Na h-Eileanan Siar":
			            			   sb.append("Na h-Eileanan an Iar");
			            			   break;
			            		   case "Caithness":
			            			   sb.append("Highland");
			            			   break;
			            		   case "Shapinsay":
			            			   sb.append("Orkney");
			            			   break;   
			            		   case "Oxon":
			            			   sb.append("Oxfordshire");
			            			   break;
			            		   default:
			            			   sb.append(tempCounty);
			            			   break;
		            		   }
		            		   break;
		            	   }
		               }
		               
		               
		               if(eElement.getElementsByTagName("state").item(0)!= null)
		               { 
		            	   if(eElement.getElementsByTagName("state").item(0).getTextContent().matches("Wales"))
		            	   {
		            		   if(eElement.getElementsByTagName("city").item(0) != null)
		            		   {
		            			   String city = eElement.getElementsByTagName("city").item(0).getTextContent();
		            			   
		            			   String clwyd = "Conwy, Denbighshire, Flintshire, Wrexham";
		            			   String dyfed = "Carmarthenshire, Ceredigion, Pembrokeshire";
		            			   String gwynedd = "Gwynedd, Isle of Anglesey";
		            			   String midGlamorgan = "Bridgend, Merthyr Tydfil, Rhondda Cynon Taf";
		            			   String powys = "Powys";
		            			   String southGlamorgan = "Cardiff, Vale of Glamorgan";
		            			   String westGlamorgan = "Neath Port Talbot, Swansea";
		            			   
		            			   if(clwyd.contains(city))
		            			   {
		            				   sb.append("Clwyd");
		            				   break;
		            			   }
		            			   else if(dyfed.contains(city))
		            			   {
		            				   sb.append("Dyfed");
		            				   break;
		            			   }
		            			   else if(gwynedd.contains(city)) 
		            			   {
		            				   sb.append("Gwynedd");
		            				   break;
		            			   }
		            			   else if(powys.contains(city))
		            			   {
		            				   sb.append("Powys");
		            				   break;
		            			   }
		            			   else if(southGlamorgan.contains(city)) 
		            			   {
		            				   sb.append("South Glamorgan");
		            				   break;
		            			   }
		            			   else if(westGlamorgan.contains(city)) 
		            			   {
		            				   sb.append("West Glamorgan");
		            				   break;
		            			   }
		            			   else if(midGlamorgan.contains(city)) 
		            			   {		
		            				   sb.append("Mid Glamorgan");
		            				   break;
		            			   }
		            		   }
		            	   }
		               }
		               		               
		               
		               if(eElement.getElementsByTagName("city").item(0)!= null)
	            	   {
		            	   if(checkCounty(eElement.getElementsByTagName("city").item(0).getTextContent()))
		            	   {
		            		   if(eElement.getElementsByTagName("city").item(0).getTextContent().matches("London"))
            				   {
            					   sb.append("Greater London");
            					   break;
            				   }
            				   else
            				   {
            					   sb.append(eElement.getElementsByTagName("city").item(0).getTextContent());				            			   
		            			   break;
            				   }
		            	   }
	            	   }
		               
		               
		               if(eElement.getElementsByTagName("town").item(0) != null)
		               {
		            	   if(checkCounty(eElement.getElementsByTagName("town").item(0).getTextContent()))
		            	   {
		            		   if(eElement.getElementsByTagName("town").item(0).getTextContent().matches("London"))
            				   {
            					   sb.append("Greater London");
            					   break;
            				   }
            				   else
            				   {
            					   //sb.append(eElement.getElementsByTagName("town").item(0).getTextContent());
            					   sb.append("null");
		            			   break;
            				   }
		            	   }
		               }
		               
		               if(eElement.getElementsByTagName("village").item(0) != null)
		               {
		            	   if(checkCounty(eElement.getElementsByTagName("village").item(0).getTextContent()))
		            	   {
		            		   if(eElement.getElementsByTagName("village").item(0).getTextContent().matches("London"))
            				   {
            					   sb.append("Greater London");
            					   break;
            				   }
            				   else
            				   {
            					   //sb.append(eElement.getElementsByTagName("village").item(0).getTextContent());	
            					   sb.append("null");
		            			   break;
            				   }
		            	   }
		               }
		               sb.append("null"); // county city town village not present cannot determine main county
		            }
				}			
	         }
			String tempStr = sb.toString();
			strContainer.add(tempStr);
		}
		
		
		
		
		PrintWriter pw;
		PrintWriter pwT;
		try {
			pw = new PrintWriter(new File("G:\\PC\\Desktop\\tempF.csv"));
			pwT = new PrintWriter(new File("G:\\PC\\Desktop\\null.csv"));
			for(int k = 0; k < docContainer.size();k++)
			{
				StringBuilder temp = new StringBuilder();
				temp.append(origData.get(k));
				temp.append(",");
				temp.append(strContainer.get(k));
				temp.append(",");
				temp.append("A"+k);
				temp.append("\n");
				
				String finalString = temp.toString();
				System.out.println(finalString);
				if(!finalString.contains("null"))
				{
					pw.write(finalString);
					System.out.println("Written: "+ k);
				}
				else
				{
					pwT.write(finalString);
				}
				
			}
			pw.close();
			
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
	
}


