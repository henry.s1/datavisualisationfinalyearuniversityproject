package test;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URL;
import java.util.ArrayList;

public class projectGet
{
	private static ArrayList<String> getData(String fileName, int i)
	{
		
		ArrayList<String> tempContainer = new ArrayList<String>();
		
		BufferedReader br = null;
		
		try
		{
			br = new BufferedReader(new FileReader(fileName));
			
			String line ="";
			br.readLine();
			while((line = br.readLine()) != null)
			{
				if(line.contains("﻿"))
				{
					line = line.replace("﻿", "");
				}
				tempContainer.add(line);	
			}				
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
		finally
		{
			try
			{
				br.close();
			}
			catch(IOException ioe)
			{
				System.out.println("Error occured while closing the BufferedReader");
                ioe.printStackTrace();
			}
		}
				
		return tempContainer;
	}
	
	private static void copy(InputStream in, OutputStream out) throws IOException
	{
		byte[] buffer = new byte[1024];
		while(true)
		{
			int readCount = in.read(buffer);
			if(readCount == -1)
			{
				break;
			}
			out.write(buffer, 0, readCount);
		}
	}
	
	@SuppressWarnings("unused")
	private static void download(URL input, File output) throws IOException
	{
		InputStream in = input.openStream();
		try
		{
			OutputStream out = new FileOutputStream(output);
			try
			{
				copy(in, out);
			}
			finally
			{
				out.close();
			}
		}
		finally
		{
			in.close();
		}
	}
	
	public static void main(String[] args)
	{
		ArrayList<String> latitude = getData("G:\\PC\\Desktop\\test\\1\\acc.csv", 4);
		ArrayList<String> longitude = getData("G:\\PC\\Desktop\\test\\1\\acc.csv", 3);
		
		
		
		
		
		
		
		
		
		

	}

}
